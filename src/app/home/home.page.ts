import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  slideOpts = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay: true,
  };
  slideList = { slidesPerView: 3.2, slideShadows: true };
  dummyBanners: any[] = [];
  banners: any[] = [];
  catIcons: any;
  constructor() {
    this.banners = [
      { message: 'aaa', cover: 'assets/slider/1.jpeg' },
      { message: 'aaa', cover: 'assets/slider/2.jpeg' },
      { message: 'aaa', cover: 'assets/slider/3.jpeg' },
    ];
    //this.dummyBanners = Array(5);

    this.catIcons = [
      { name: 'Action', icon: 'assets/chacha.png' },
      { name: 'Action', icon: 'assets/chacha.png' },

      { name: 'Action', icon: 'assets/chacha.png' },
      { name: 'Action', icon: 'assets/chacha.png' },
      { name: 'Action', icon: 'assets/chacha.png' },

      { name: 'Action', icon: 'assets/chacha.png' },
      { name: 'Action', icon: 'assets/chacha.png' },
      { name: 'Action', icon: 'assets/chacha.png' },

      { name: 'Action', icon: 'assets/chacha.png' },
    ];
  }

  ngOnInit() {}
}
